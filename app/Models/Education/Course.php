<?php

namespace App\Models\Education;

use App\Models\Profiles\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'visible'
    ];

    public function students(){
        return $this->belongsToMany(User::class,'accesses');
    }

    public function chapters(){
        return $this->hasMany(Chapter::class,'course_id','id');
    }
}
