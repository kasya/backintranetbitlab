<?php

namespace App\Models\Education;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $fillable = [
        'lesson_type_id',
        'chapter_id',
        'content',
        'name',
    ];

    public function lessonType()
    {
        return $this->belongsTo(LessonType::class, 'lesson_type_id', 'id');
    }

    public function chapter()
    {
        return $this->belongsTo(Chapter::class, 'chapter_id', 'id');
    }
}
