<?php

namespace App\Models\Education;

use Illuminate\Database\Eloquent\Model;

class LessonType extends Model
{
    protected $table = "lesson_types";

    public const LECTURE = 1;
    public const PRACTICE = 2;
    public const LAB_WORK = 3;

    protected $fillable = [
        'name'
    ];

    public function lessons()
    {
        return $this->hasMany(Lesson::class, 'lesson_type_id', 'id');
    }
}
