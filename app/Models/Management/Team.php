<?php

namespace App\Models\Management;

use App\Models\Profiles\User;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $guarded = [];

    public function coach(){
        return $this->hasOne(User::class,'id','coach_id');
    }
}
