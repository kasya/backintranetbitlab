<?php

namespace App\Models\Management;

use App\Models\Education\Course;
use App\Models\Profiles\User;
use Illuminate\Database\Eloquent\Model;

class Access extends Model
{
    protected $fillable = [
        'course_id',
        'user_id'
    ];

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
