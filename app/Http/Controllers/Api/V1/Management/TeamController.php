<?php
/**
 * Created by PhpStorm.
 * User: eldarkhasen
 * Date: 3/24/20
 * Time: 22:40
 */

namespace App\Http\Controllers\Api\V1\Management;


use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\Api\V1\Management\StoreAndUpdateTeamApiRequest;
use App\Models\Management\Team;

class TeamController extends ApiBaseController
{
    public function index()
    {
        return $this->successResponse(Team::with('coach')->paginate(20));
    }


    public function store(StoreAndUpdateTeamApiRequest $request){
        return $this->successResponse(Team::create($request->all()));
    }

    public function update($id,StoreAndUpdateTeamApiRequest $request){
        $team = Team::findOrFail($id);
        return $this->successResponse($team->update($request->all()));
    }

    public function getById($id){
        $team = Team::findOrFail($id);
        return $this->successResponse($team);
    }

}