<?php
/**
 * Created by PhpStorm.
 * User: eldarkhasen
 * Date: 3/23/20
 * Time: 02:33
 */

namespace App\Http\Controllers\Api\V1\Management;


use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\Api\V1\Management\StoreAndUpdateStudentApiRequest;
use App\Http\Requests\Api\V1\Management\StoreAndUpdateUserApiRequest;
use App\Models\Education\Course;
use App\Models\Profiles\Role;
use App\Models\Profiles\User;

class UserController extends ApiBaseController
{
    public function index()
    {
        return $this->successResponse(User::with(["role"])->paginate(20));
    }

    public function store(StoreAndUpdateUserApiRequest $request)
    {
        return $this->successResponse(User::create([
            'name' => $request->name,
            'email' => $request->email,
            'role_id' => $request->role_id,
            'password' => bcrypt($request->password),
        ]));
    }

    public function update($id, StoreAndUpdateUserApiRequest $request)
    {
        $user = User::findOrFail($id);
        if(isset($request->password)){
            return $this->successResponse($user->update([
                'name' => $request->name,
                'email' => $request->email,
                'role_id' => $request->role_id,
                'password' => bcrypt($request->password),
            ]));
        }else{
            return $this->successResponse($user->update([
                'name' => $request->name,
                'email' => $request->email,
                'role_id' => $request->role_id,
            ]));
        }

    }

    public function getRoles()
    {
        return $this->successResponse(Role::all());
    }

    public function getById($id)
    {
        return $this->successResponse(User::findOrFail($id));
    }

    public function getOnlyCoaches(){
        $users = User::where('id','=',Role::TEACHER_ID)->get();
        return $this->successResponse($users);
    }

    public function getStudents($id){
        $course = Course::findOrFail($id);
        return $this->successResponse($course->students()->paginate(20));

    }

    public function getNotRelatedStudents($id){
        $students = User::where('role_id','=',Role::STUDENT_ID)->whereDoesntHave('courses', function($q) use ($id){
            $q->where('course_id', $id);
        })->paginate(20);

        return $this->successResponse($students);

    }

    public function addToCourse($id,  StoreAndUpdateStudentApiRequest $request){
        $course = Course::findOrFail($id);
        return $this->successResponse($course->students()->attach($request->id));

    }

    public function deleteFromCourse($id,  StoreAndUpdateStudentApiRequest $request){
        $course = Course::findOrFail($id);
        return $this->successResponse($course->students()->detach($request->id));
    }

}