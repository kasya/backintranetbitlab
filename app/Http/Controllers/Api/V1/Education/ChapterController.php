<?php
/**
 * Created by PhpStorm.
 * User: air
 * Date: 27.03.2020
 * Time: 08:55
 */

namespace App\Http\Controllers\Api\V1\Education;


use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\Api\V1\Chapter\StoreAndUpdateChapterApiRequest;
use App\Models\Education\Chapter;
use App\Services\v1\ChapterService;

class ChapterController extends ApiBaseController
{

    protected $chapterService;

    /**
     * ChapterController constructor.
     * @param $chapterService
     */
    public function __construct(ChapterService $chapterService)
    {
        $this->chapterService = $chapterService;
    }


    public function index($course_id)
    {
        return $this->successResponse($this->chapterService->chaptersByCourseIdPaginated($course_id, 10));
    }

    public function store($course_id, StoreAndUpdateChapterApiRequest $request)
    {
        $chapter = new Chapter();
        $chapter->fill($request->all());
        $chapter->course_id = $course_id;
        $chapter->save();
        return $this->successResponse($chapter);
    }

    public function update($id, StoreAndUpdateChapterApiRequest $request)
    {
        $chapter = Chapter::findOrFail($id);
        $chapter->fill($request->all());
        $chapter->save();
        return $this->successResponse($chapter);
    }

    public function getById($id)
    {
        return $this->successResponse(Chapter::with('course')->findOrFail($id));
    }
}