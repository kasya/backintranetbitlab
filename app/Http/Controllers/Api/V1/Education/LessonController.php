<?php
/**
 * Created by PhpStorm.
 * User: air
 * Date: 27.03.2020
 * Time: 15:00
 */

namespace App\Http\Controllers\Api\V1\Education;


use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\Api\V1\Lesson\StoreAndUpdateLessonApiRequest;
use App\Models\Education\Lesson;
use App\Services\v1\LessonService;

class LessonController extends ApiBaseController
{

    protected $lessonService;

    /**
     * ChapterController constructor.
     * @param $chapterService
     */
    public function __construct(LessonService $chapterService)
    {
        $this->lessonService = $chapterService;
    }


    public function index($chapter_id)
    {
        return $this->successResponse($this->lessonService->lessonsByChapterId($chapter_id));
    }

    public function store(StoreAndUpdateLessonApiRequest $request)
    {
        $lesson = new Lesson();
        $lesson->fill($request->all());
        $lesson->save();
        return $this->successResponse($lesson);
    }

    public function update($id, StoreAndUpdateLessonApiRequest $request)
    {
        $lesson = Lesson::findOrFail($id);
        $lesson->fill($request->all());
        $lesson->save();
        return $this->successResponse($lesson);
    }

    public function getById($id)
    {
        return $this->successResponse(Lesson::with('chapter')->findOrFail($id));
    }

}