<?php
/**
 * Created by PhpStorm.
 * User: air
 * Date: 14.03.2020
 * Time: 21:40
 */

namespace App\Http\Controllers\Api\V1\Education;


use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\Api\V1\Course\StoreAndUpdateCourseApiRequest;
use App\Http\Resources\CourseResource;
use App\Models\Education\Course;
use App\Models\Profiles\User;
use App\Services\v1\CourseService;
use Illuminate\Support\Facades\Auth;

class CourseController extends ApiBaseController
{
    protected $courseService;

    /**
     * CourseController constructor.
     * @param $courseService
     */
    public function __construct(CourseService $courseService)
    {
        $this->courseService = $courseService;
    }

    public function getUsersCourses($user_id)
    {
        $user = User::find($user_id);
        $courses = $user->courses()->with([
            'chapters',
            'chapters.lessons'
        ])->get();
//        $courses = Course::with([
//            'chapters',
//            'chapters.lessons'
//        ])->get();
        return $this->successResponse(CourseResource::collection($courses));
    }


    public function index()
    {
        return $this->successResponse($this->courseService->getAllPaginated(10));
    }

    public function store(StoreAndUpdateCourseApiRequest $request)
    {
        return $this->successResponse(Course::create($request->all()));
    }

    public function update($id, StoreAndUpdateCourseApiRequest $request)
    {
        $course = Course::findOrFail($id);
        return $this->successResponse($course->update($request->all()));
    }

    public function getById($id)
    {
        $course = Course::findOrFail($id);
        return $this->successResponse($course);
    }

    public function myCourses()
    {
        return $this->successResponse($this->courseService->studentsCourses(Auth::id()));
    }
}