<?php
/**
 * Created by PhpStorm.
 * User: air
 * Date: 27.03.2020
 * Time: 08:51
 */

namespace App\Http\Controllers\Api\V1\Education;


use App\Http\Controllers\ApiBaseController;
use App\Models\Education\LessonType;

class LessonTypeController extends ApiBaseController
{
    public function index()
    {
        return $this->successResponse(LessonType::all());
    }
}