<?php
/**
 * Created by PhpStorm.
 * User: air
 * Date: 23.03.2020
 * Time: 01:43
 */

namespace App\Http\Requests\Api\V1\Course;


use App\Http\Requests\Api\ApiBaseRequest;

class StoreAndUpdateCourseApiRequest extends ApiBaseRequest
{
    public function injectedRules()
    {
        return [
            'name' => ['required', 'string'],
            'description' => ['required', 'string'],
            'visible' => ['required', 'boolean'],
        ];
    }

}