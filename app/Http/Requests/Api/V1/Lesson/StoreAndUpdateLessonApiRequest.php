<?php
/**
 * Created by PhpStorm.
 * User: air
 * Date: 27.03.2020
 * Time: 15:04
 */

namespace App\Http\Requests\Api\V1\Lesson;


use App\Http\Requests\Api\ApiBaseRequest;

class StoreAndUpdateLessonApiRequest extends ApiBaseRequest
{
    public function injectedRules()
    {
        return [
            'name' => ['required', 'string'],
            'content' => ['required', 'string'],
            'chapter_id' => ['required', 'numeric', 'exists:chapters,id'],
            'lesson_type_id' => ['required', 'numeric', 'exists:lesson_types,id'],
        ];
    }

}