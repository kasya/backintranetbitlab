<?php
/**
 * Created by PhpStorm.
 * User: air
 * Date: 27.03.2020
 * Time: 09:04
 */

namespace App\Http\Requests\Api\V1\Chapter;


use App\Http\Requests\Api\ApiBaseRequest;

class StoreAndUpdateChapterApiRequest extends ApiBaseRequest
{
    public function injectedRules()
    {
        return [
            'name' => ['required'],
            'description' => ['required']
        ];
    }

}