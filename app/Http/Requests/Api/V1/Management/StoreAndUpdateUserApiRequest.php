<?php
/**
 * Created by PhpStorm.
 * User: eldarkhasen
 * Date: 3/23/20
 * Time: 02:35
 */

namespace App\Http\Requests\Api\V1\Management;


use App\Http\Requests\Api\ApiBaseRequest;
use App\Models\Profiles\User;

class StoreAndUpdateUserApiRequest extends ApiBaseRequest
{
    public function injectedRules(){
        $id = $this->route('id');
        if($id){
            return [
                'id'=>['required', 'numeric'],
                'name' => ['required', 'string'],
                'email' => ['required', 'email', "unique:users,email,$id"],
                'role_id'=>['required', 'numeric']
            ];
        }else{
            return [
                'password' => ['required', 'min:8'],
                'name' => ['required', 'string'],
                'email' => ['required', 'email', 'unique:users'],
                'role_id'=>['required', 'numeric']
            ];
        }

    }
}