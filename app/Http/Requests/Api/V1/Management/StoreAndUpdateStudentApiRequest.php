<?php
/**
 * Created by PhpStorm.
 * User: eldarkhasen
 * Date: 3/27/20
 * Time: 21:20
 */

namespace App\Http\Requests\Api\V1\Management;


use App\Http\Requests\Api\ApiBaseRequest;

class StoreAndUpdateStudentApiRequest extends ApiBaseRequest
{
    public function injectedRules()
    {
        return [
            'id'=>['required', 'numeric'],
            'name' => ['required', 'string']
        ];
    }
}