<?php

namespace App\Providers;

use App\Services\v1\impl\AuthServiceImpl;
use App\Services\v1\impl\ChapterServiceImpl;
use App\Services\v1\impl\CourseServiceImpl;
use App\Services\v1\impl\FileServiceImpl;
use App\Services\v1\impl\LessonServiceImpl;
use Illuminate\Support\ServiceProvider;

class SystemServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\v1\FileService', function ($app) {
            return (new FileServiceImpl());
        });

        $this->app->bind('App\Services\v1\AuthService', function ($app) {
            return (new AuthServiceImpl());
        });

        $this->app->bind('App\Services\v1\ChapterService', function ($app) {
            return (new ChapterServiceImpl());
        });

        $this->app->bind('App\Services\v1\CourseService', function ($app) {
            return (new CourseServiceImpl());
        });

        $this->app->bind('App\Services\v1\LessonService', function ($app) {
            return (new LessonServiceImpl());
        });


    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
