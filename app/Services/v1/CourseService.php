<?php
/**
 * Created by PhpStorm.
 * User: air
 * Date: 14.03.2020
 * Time: 22:28
 */

namespace App\Services\v1;


interface CourseService
{
    public function getAllPaginated($pageSize);

    public function studentsCourses($userId);
}