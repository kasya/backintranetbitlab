<?php
/**
 * Created by PhpStorm.
 * User: air
 * Date: 27.03.2020
 * Time: 15:01
 */

namespace App\Services\v1;


interface LessonService
{
    function lessonsByChapterId($chapterId);
}