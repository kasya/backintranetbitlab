<?php
/**
 * Created by PhpStorm.
 * User: air
 * Date: 27.03.2020
 * Time: 15:01
 */

namespace App\Services\v1\impl;


use App\Models\Education\Lesson;
use App\Services\v1\LessonService;

class LessonServiceImpl implements LessonService
{
    function lessonsByChapterId($chapterId)
    {
        return Lesson::where('chapter_id', $chapterId)->with(['lessonType', 'chapter'])->get();
    }

}