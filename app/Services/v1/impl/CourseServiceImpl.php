<?php
/**
 * Created by PhpStorm.
 * User: air
 * Date: 14.03.2020
 * Time: 22:28
 */

namespace App\Services\v1\impl;


use App\Models\Education\Course;
use App\Services\v1\CourseService;

class CourseServiceImpl implements CourseService
{
    public function getAllPaginated($pageSize)
    {
        return Course::paginate($pageSize);
    }

    public function studentsCourses($userId)
    {
        return Course::distinct()
            ->select(['courses.*'])
            ->join('accesses as a', 'a.course_id', '=', 'courses.id')
            ->where('a.user_id', '=', $userId)
            ->get();
    }

}