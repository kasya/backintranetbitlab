<?php
/**
 * Created by PhpStorm.
 * User: air
 * Date: 27.03.2020
 * Time: 08:58
 */

namespace App\Services\v1\impl;


use App\Models\Education\Chapter;
use App\Services\v1\ChapterService;

class ChapterServiceImpl implements ChapterService
{
    public function chaptersByCourseIdPaginated($id, $pageSize = 10)
    {
        return Chapter::where('course_id', $id)->with('course')->paginate($pageSize);
    }

}