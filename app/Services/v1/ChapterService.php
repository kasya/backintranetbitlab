<?php
/**
 * Created by PhpStorm.
 * User: air
 * Date: 27.03.2020
 * Time: 08:58
 */

namespace App\Services\v1;


interface ChapterService
{
    public function chaptersByCourseIdPaginated($id, $pageSize = 10);
}