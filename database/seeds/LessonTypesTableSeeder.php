<?php

use Illuminate\Database\Seeder;

class LessonTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Education\LessonType::create([
            'name' => 'Лекция'
        ]);
        \App\Models\Education\LessonType::create([
            'name' => 'Практика'
        ]);
        \App\Models\Education\LessonType::create([
            'name' => 'Лабораторная работа'
        ]);
    }
}
