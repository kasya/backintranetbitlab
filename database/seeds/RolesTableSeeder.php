<?php

use Illuminate\Database\Seeder;
use \App\Models\Profiles\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'Admin',
        ]);

        Role::create([
            'name' => 'Teacher',
        ]);

        Role::create([
            'name' => 'Student',
        ]);
    }
}
