<?php

use \App\Models\Profiles\Role;

Route::group(['middleware' => 'api'], function () {


    Route::group(['namespace' => 'Auth'], function () {
        Route::get('/login', ['uses' => 'AuthController@authFail', 'as' => 'login']);
        Route::post('/login', ['uses' => 'AuthController@login']);
        Route::post('/register', ['uses' => 'AuthController@register']);
    });

    //AUTHENTICATED
    Route::group(['middleware' => 'auth:api'], function () {

        Route::group(['namespace' => 'Auth'], function () {
            Route::post('/me', ['uses' => 'AuthController@me']);
            Route::get('/get-users-courses/{id}', ['uses' => 'CourseController@getUsersCourses']);
        });

        Route::group(['namespace' => 'Education'], function () {
            Route::get('/lesson-types', ['uses' => 'LessonTypeController@index']);

            Route::group(['middleware' => ['ROLE_OR:' . Role::ADMIN_ID . ',' . Role::TEACHER_ID]], function () {
                Route::get('/courses', ['uses' => 'CourseController@index']);
                Route::get('/courses/{id}', ['uses' => 'CourseController@getById'])->where('id', '[0-9]+');
                Route::post('/courses', ['uses' => 'CourseController@store']);
                Route::put('/courses/{id}', ['uses' => 'CourseController@update'])->where('id', '[0-9]+');

                Route::get('/chapters/by-course/{course_id}', ['uses' => 'ChapterController@index'])->where('course_id', '[0-9]+');
                Route::post('/chapters/{course_id}', ['uses' => 'ChapterController@store'])->where('course_id', '[0-9]+');
                Route::put('/chapters/{id}', ['uses' => 'ChapterController@update'])->where('id', '[0-9]+');
                Route::get('/chapters/{id}', ['uses' => 'ChapterController@getById'])->where('id', '[0-9]+');

                Route::get('/lessons/by-chapter/{chapter_id}', ['uses' => 'LessonController@index'])->where('chapter_id', '[0-9]+');
                Route::post('/lessons', ['uses' => 'LessonController@store']);
                Route::put('/lessons/{id}', ['uses' => 'LessonController@update'])->where('id', '[0-9]+');
                Route::get('/lessons/{id}', ['uses' => 'LessonController@getById'])->where('id', '[0-9]+');
            });

            Route::group(['middleware' => ['ROLE_OR:' . Role::STUDENT_ID]], function () {
                Route::get('/my/courses', ['uses' => 'CourseController@myCourses']);
            });
        });

        Route::group(['namespace' => 'Management'], function () {

            Route::group(['middleware' => ['ROLE_OR:' . Role::ADMIN_ID]], function () {
                Route::get('/users', ['uses' => 'UserController@index']);
                Route::get('/users/by-course/{id}', ['uses' => 'UserController@getStudents'])->where('course_id', '[0-9]+');
                Route::get('/users/not-related-to-course/{id}', ['uses' => 'UserController@getNotRelatedStudents'])->where('course_id', '[0-9]+');
                Route::get('/users/{id}', ['uses' => 'UserController@getById'])->where('id', '[0-9]+');
                Route::post('/users', ['uses' => 'UserController@store']);
                Route::post('/users/add-to-course/{id}', ['uses' => 'UserController@addToCourse']);
                Route::post('/users/delete-from-course/{id}', ['uses' => 'UserController@deleteFromCourse']);
                Route::put('/users/{id}', ['uses' => 'UserController@update'])->where('id', '[0-9]+');
                Route::get('/roles', ['uses' => 'UserController@getRoles']);
            });

            Route::group(['middleware' => ['ROLE_OR:' . Role::ADMIN_ID . ',' . Role::TEACHER_ID]], function () {

                Route::get('/coaches', ['uses' => 'UserController@getOnlyCoaches']);
                Route::get('/teams', ['uses' => 'TeamController@index']);
                Route::get('/teams/{id}', ['uses' => 'TeamController@getById'])->where('id', '[0-9]+');
                Route::post('/teams', ['uses' => 'TeamController@store']);
                Route::put('/teams/{id}', ['uses' => 'TeamController@update'])->where('id', '[0-9]+');
            });
        });
    });

});
